# booklet-scripts

## Before using

### Install dependencies

The PoDoFo tools are the only requirement. On Debian/Ubuntu, you'll need to install the `libpodofo-tools` package:

```bash
sudo apt-get install libpodofo-tools
```

### Link the scripts for easy access

It's useful to place symlinks in a directory on your `$PATH` so that you can call them from anywhere. If `~/bin` is on your `$PATH`, just type

```bash
ln -s `pwd`/booklet-* ~/bin
```

Alternatively you can link them system-wide by putting the links in `/usr/local/bin`:

```bash
sudo ln -s `pwd`/booklet-* /usr/local/bin
```


## booklet-impose-a5

A script to turn an A5 PDF into a set of 2-up A4 pages for double-sided printing. It's a way to quickly perform imposition so we can print A5 booklets in a double-sided A4 printer so that they come out ready to be folded and stapled.

The script uses `podofoimpose` with a `.plan` file with the imposition logic, which is directly based on an [original recipe](https://web.archive.org/web/20160525155646/http://stdin.fr/Bazar/PoDoFo) by [Pierre Marchand](https://monoskop.org/Pierre_Marchand), the author of `podofoimpose`. We are also indebted to [stdin](http://stdin.fr) for pointing the right way.

Read the `.plan` file source for an explanation of the logic to arrange and sort the pages so that they're laid out in the correct order.

### Usage

```bash
booklet-impose-a5 source.pdf
```

and it will create `source-a4.pdf`.


## booklet-get-page-total

Returns the total number of pages on a PDF file.

```bash
$ booklet-get-page-total source.pdf
24
```

## booklet-insert-blank-page

Inserts a blank page on the specified page. The syntax is

```bash
booklet-insert-blank-page <file.pdf> <format> <page_number>
```

where `<format>` should be a paper size (see the [Ghostscript paper sizes resource](https://www.ghostscript.com/doc/current/Use.htm#Known_paper_sizes) for accepted formats), and `<page_number>` can either be a page number, or the strings `start` or `end`. The blank page will be inserted at the specified page number, so if you specify page 3, then page 3 will now be a blank page, and what used to be page 3 will now be page 4.

Note that **this overwrites the source PDF file**!

Example usage:

```bash
# insert an A5 blank page at page 3
booklet-insert-blank-page source.pdf a5 3
# insert an A4 blank page at the end of the document
booklet-insert-blank-page source.pdf a4 end
# insert an A4 blank page at the beginning of the document
booklet-insert-blank-page source.pdf a4 start
# which is the same as
booklet-insert-blank-page source.pdf a4 1
```



